package pw.nerde.bullinkoth

import com.github.andriykuba.scala.glicko2.scala.Glicko2
import com.github.andriykuba.scala.glicko2.scala.Glicko2.{
  Player => GPlayer,
  Win,
  Loss,
  Draw
}
import java.io.{File, PrintWriter}
import scala.io.Source

import pw.nerde.bullinkoth.htmlgen._

case class Player(name: String, rating: GPlayer, score: Int)

object Main extends App {
  val ratingMap = Map(
    Source
      .fromFile("rating.txt")
      .getLines
      .map { line =>
        val data = line.split(" ")
        val rating =
          GPlayer(data(1).toDouble, data(2).toDouble, data(3).toDouble)
        data(0) -> Player(data(0), rating, data(4).toInt)
      }
      .toSeq: _*)
  val results =
    (for {
      p1 <- ratingMap.keys
      p2 <- ratingMap.keys if p1 != p2
    } yield {
      val res = OthelloBattle(p1, p2)
      (p1, p2, res)
    }).toSeq
  val resultMap = Map(results.map(t => (t._1, t._2) -> t._3): _*)
  val toRes = (s: Int, p: GPlayer) =>
    if (s == 1) Win(p) else if (s == -1) Loss(p) else Draw(p)
  val updatedRating =
    (for {
      (name, player) <- ratingMap
    } yield {
      val p =
        Glicko2.update(
          player.rating,
          results.collect {
            case (cur, oppo, score) if cur == name =>
              toRes(score, ratingMap(oppo).rating)
            case (oppo, cur, score) if cur == name =>
              toRes(-score, ratingMap(oppo).rating)
          }
        )
      val score = results.foldLeft(player.score) {
        case (acc, (cur, oppo, score)) if cur == name => acc + score
        case (acc, (oppo, cur, score)) if cur == name => acc - score
        case (acc, _)                                 => acc
      }
      Player(name, p, score)
    }).toSeq
  val ratingStr = updatedRating
    .map(p =>
      s"${p.name} ${p.rating.rating} ${p.rating.deviation} ${p.rating.volatility} ${p.score}")
    .mkString("\n")
  val ratingWriter = new PrintWriter(new File("rating.txt"))
  ratingWriter.write(ratingStr)
  ratingWriter.close()
  val sbWriter = new PrintWriter(new File("scoreboard.html"))
  sbWriter.write(Scoreboard(updatedRating))
  sbWriter.close()
  val resWriter = new PrintWriter(new File("results.html"))
  resWriter.write(ResultMatrix(updatedRating, resultMap))
  resWriter.close()
}
