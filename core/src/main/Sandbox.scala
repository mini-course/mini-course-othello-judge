package pw.nerde.bullinkoth

import java.io.{BufferedReader, InputStream, InputStreamReader, OutputStream}
import scala.concurrent.{Await, Future, SyncVar, TimeoutException, blocking}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.sys.process._
import scala.util.Try

object Sandbox {
  def runOneshot(exe: String,
                 home: String,
                 cgroup: CGroup,
                 timelimit: Long,
                 memlimit: Long): Int = {
    cgroup.setMemLimit(memlimit)
    val p = cgroup
      .exec(
        s"firejail --quiet --profile=sandbox.profile --private=${home} -- ${exe}")
      .run()
    val f = Future(blocking(p.exitValue()))
    val t0 = cgroup.getCpuTime()
    Try {
      val r = Await.result(f, (timelimit * 2).nanos)
      val t1 = cgroup.getCpuTime()
      if (t1 - t0 <= timelimit) p.exitValue else -1
    }.getOrElse({ p.destroy(); -1 })
  }

  def apply(exe: String,
            home: String,
            cgroup: CGroup,
            memlimit: Long): Sandbox = {
    System.err.println(s"[Info] Starting process in ${home}")
    cgroup.setMemLimit(memlimit)
    cgroup.thaw()
    val pb = Process(cgroup.exec(
      s"firejail --quiet --profile=sandbox.profile --private=${home} -- ${exe}"))
    val is = new SyncVar[OutputStream]
    val os = new SyncVar[InputStream]
    val io =
      new ProcessIO(
        stdin => is.put(stdin),
        stdout => os.put(stdout),
        stderr =>
          scala.io.Source.fromInputStream(stderr).getLines.foreach(println))
    val proc = pb.run(io)
    val br = new BufferedReader(new InputStreamReader(os.get))
    cgroup.freeze()
    new Sandbox(is, os, br, proc, cgroup)
  }
}

class Sandbox(private val is: SyncVar[OutputStream],
              private val os: SyncVar[InputStream],
              private val br: BufferedReader,
              private val proc: Process,
              private val cgroup: CGroup) {
  def readLine(timeout: Long): Try[String] = {
    cgroup.thaw()
    val f = Future(blocking(br.readLine()))
    val t0 = cgroup.getCpuTime()
    val ret = Try {
      val r = Await.result(f, (timeout * 2).nanos)
      val t1 = cgroup.getCpuTime()
      // TODO: Flatten instead of throw
      if (t1 - t0 <= timeout) {
        //System.err.println(s"[Info] Read: ${r} / Time: ${(t1 - t0) / 1000000}")
        r
      } else {
        proc.destroy()
        throw new TimeoutException("Cpu time limit exceeded")
      }
    }
    cgroup.freeze()
    ret
  }

  def writeLine(line: String): Unit = {
    //System.err.println(s"[Info] Write: ${line}")
    is.get.write((line + "\n").getBytes)
    is.get.flush()
  }

  def kill(): Unit = proc.destroy()
}
