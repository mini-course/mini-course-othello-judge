package pw.nerde.bullinkoth

object Othello {
  val Empty = 0
  val Black = 1
  val White = 2

  val initGame: Othello = {
    val empty = Seq.fill(8)(Seq.fill(8)(Empty))
    val board = Seq((3, 3, White), (3, 4, Black), (4, 3, Black), (4, 4, White))
      .foldLeft(empty)((b, p) => updatedBoard(p._1, p._2, p._3, b))
    Othello(board, Black)
  }

  def inBoard(x: Int, y: Int): Boolean = 0 <= x && x < 8 && 0 <= y && y < 8

  def updatedBoard(x: Int,
                   y: Int,
                   to: Int,
                   oldBoard: Seq[Seq[Int]]): Seq[Seq[Int]] =
    oldBoard.updated(x, oldBoard(x).updated(y, to))
}

case class Othello(board: Seq[Seq[Int]],
                   turn: Int,
                   lastPlayerPass: Boolean = false) {
  assert(board.size == 8)
  assert(board.forall(_.size == 8))

  lazy val canPlay: Boolean = {
    (0 until 8)
      .flatMap { xx =>
        (0 until 8).map(yy => flipPos(xx, yy).nonEmpty)
      }
      .exists(_ == true)
  }

  def end: Boolean = lastPlayerPass && !canPlay

  def finalScore: Int = {
    val black = board.foldLeft(0)(_ + _.count(_ == Othello.Black))
    val white = board.foldLeft(0)(_ + _.count(_ == Othello.White))
    if (black < white) -1
    else if (black > white) 1
    else 0
  }

  def play(x: Int, y: Int): Othello = {
    val pos = flipPos(x, y)
    require(pos.nonEmpty)
    val flipped =
      pos.foldLeft(board)((b, p) => Othello.updatedBoard(p._1, p._2, turn, b))
    val result = Othello.updatedBoard(x, y, turn, flipped)
    val attempt = Othello(result, 3 - turn)
    if (attempt.canPlay) attempt else Othello(result, turn, true)
  }

  def flipPos(x: Int, y: Int): Seq[(Int, Int)] = {
    if (board(x)(y) == Othello.Empty) {
      val dir = Seq(
        (1, 0),
        (1, -1),
        (0, -1),
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, 1),
        (1, 1)
      )
      for {
        d <- dir
        s <- {
          val lis = LazyList
            .from(1)
            .map(k => (x + k * d._1, y + k * d._2))
            .takeWhile(t =>
              Othello.inBoard(t._1, t._2) && board(t._1)(t._2) == 3 - turn)
          val canRem =
            lis.lastOption
              .map(
                l =>
                  Othello.inBoard(l._1 + d._1, l._2 + d._2) &&
                    board(l._1 + d._1)(l._2 + d._2) == turn)
              .getOrElse(false)
          if (canRem) lis
          else LazyList()
        }
      } yield s
    } else {
      Seq()
    }
  }

  override def toString: String = {
    "  a b c d e f g h\n" +
      (board.zipWithIndex.map {
        case (row, id) =>
          val mid =
            row.map {
              case i if i == Othello.Black => "○"
              case i if i == Othello.White => "●"
              case _                       => " "
            } mkString " "
          s"${id} ${mid} ${id}"
      } mkString "\n") +
      "\n  a b c d e f g h\n"
  }
}
