package pw.nerde.bullinkoth.htmlgen

import scalatags.Text.all._

import pw.nerde.bullinkoth._

object ResultMatrix {
  def apply(players: Seq[Player], resultMap: Map[(String, String), Int]) =
    "<!DOCTYPE html>" + reshtml(players, resultMap).toString

  def score2Entry(score: Int) = {
    val char = if (score > 0) "O" else if (score < 0) "X" else "-"
    val col = if (score > 0) "green" else if (score < 0) "red" else "gray"
    td(color := col)(char)
  }

  def reshtml(players: Seq[Player], resultMap: Map[(String, String), Int]) =
    html(
      head(
        link(
          rel := "stylesheet",
          href := "https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.min.css")
      ),
      body(
        table(css("border-collapse") := "collapse")(
          id := "table-id",
          thead(
            tr(
              td(""),
              for (p <- players) yield td(p.name)
            )
          ),
          tbody(
            for (p <- players)
              yield
                tr(
                  td(p.name),
                  for (p2 <- players) yield {
                    if (p2 == p) td("")
                    else score2Entry(resultMap(p.name, p2.name))
                  }
                )
          )
        ),
        script(
          "new Tablesort(document.getElementById('table-id'), { descending: true, sortList: [[1, 1], [2, 0], [4, 1]] });")
      )
    )
}
