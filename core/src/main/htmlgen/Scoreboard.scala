package pw.nerde.bullinkoth.htmlgen

import scalatags.Text.all._

import pw.nerde.bullinkoth._

object Scoreboard {
  def apply(players: Seq[Player]) = "<!DOCTYPE html>" + sbhtml(players).toString

  def sbhtml(players: Seq[Player]) =
    html(
      head(
        link(
          rel := "stylesheet",
          href := "https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.min.css"),
        script(
          src := "https://cdn.jsdelivr.net/npm/tablesort@latest/src/tablesort.min.js"),
        script(
          src := "https://cdn.jsdelivr.net/npm/tablesort@latest/dist/sorts/tablesort.number.min.js")
      ),
      body(
        table(css("border-collapse") := "collapse")(
          id := "table-id",
          thead(
            tr(
              td("Team"),
              td("Rating"),
              td("Deviation"),
              td("Volatility"),
              td("Net score")
            )
          ),
          tbody(
            for (p <- players)
              yield
                tr(
                  td(p.name),
                  td(p.rating.rating.toString),
                  td(p.rating.deviation.toString),
                  td(p.rating.volatility.toString),
                  td(p.score)
                )
          )
        ),
        script(
          "new Tablesort(document.getElementById('table-id'), { descending: true, sortList: [[1, 1], [2, 0], [4, 1]] });")
      )
    )
}
