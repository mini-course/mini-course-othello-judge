package pw.nerde.bullinkoth

object Config {
  // Time limit for compilation in nanoseconds
  val CompileTl: Long = 20L << 30
  // Memory limit for compilation in bytes
  val CompileMl: Long = 4L << 30
  // Total time limit for execution in nanoseconds
  val RuntimeMl: Long = 4L << 30
  // Time limit for a turn in nanoseconds
  val TurnTl: Long = 1L << 30
  // Directory for submissions
  val BasePath = "./"
  // Directory for game logs
  val LogPath = "./"
}
