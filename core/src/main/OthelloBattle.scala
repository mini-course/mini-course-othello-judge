package pw.nerde.bullinkoth

import java.io.{File, PrintWriter}

object OthelloBattle {
  // Black: `name0`, White: `name1`
  // Returns 1 if black wins, -1 if white wins, and 0 if draw.
  def apply(name0: String, name1: String): Int = {
    val log = new PrintWriter(
      new File(Config.LogPath + s"${name0}-${name1}.log"))
    val name = Seq(name0, name1)
    val cg = Seq.fill(2)(CGroup.availible.take())
    val init = (0 to 1).map { i =>
      try {
        cg(i).thaw()
        Sandbox.runOneshot("bash build.sh",
                           Config.BasePath + name(i),
                           cg(i),
                           Config.CompileTl,
                           Config.CompileMl)
        Some(
          Sandbox(s"bash run.sh ${i}",
                  Config.BasePath + name(i),
                  cg(i),
                  Config.RuntimeMl))
      } catch {
        case e: Throwable =>
          log.write(s"Compilation error on player ${i + 1}\n")
          System.err.println(s"[Error] Fail to init on player ${i + 1}:")
          None
      }
    }
    val result = init match {
      case Seq(Some(_), None) => 1
      case Seq(None, Some(_)) => -1
      case Seq(None, None)    => 0
      case Seq(Some(s0), Some(s1)) =>
        val s = Seq(s0, s1)
        var game = Othello.initGame
        try {
          // Place custom interaction code here
          while (!game.end) {
            //System.err.println(s"[Info] Turn: ${game.turn}")
            // TODO: Clean up Try.get (and the imperative stuff around here).
            val move =
              s(game.turn - 1)
                .readLine(Config.TurnTl)
                .get
                .split(" ")
                .map(_.toInt)
            require(0 <= move(0) && move(0) < 8 && 0 <= move(1) && move(1) < 8)
            val nxtPlayer = 1 - (game.turn - 1)
            game = game.play(move(0), move(1))
            s(nxtPlayer).writeLine(move.mkString(" "))
            log.write(game.toString)
          }
          game.finalScore
        } catch {
          case e: Throwable =>
            log.write("Error on player ${game.turn}\n")
            System.err.println(s"[Error] Error on player ${game.turn}:")
            e.printStackTrace()
            // The player who caused the exception loses
            2 * (game.turn - 1) - 1
        }
    }
    log.write(s"Final score: ${result}\n")
    log.close()
    cg.foreach(_.thaw())
    init.collect { case Some(s) => s.kill() }
    cg.foreach(g => CGroup.availible.put(g))
    result
  }
}
