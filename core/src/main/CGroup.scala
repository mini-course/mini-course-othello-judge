package pw.nerde.bullinkoth

import java.util.concurrent.{BlockingQueue, LinkedBlockingQueue}
import scala.jdk.CollectionConverters._
import scala.sys.process._

object CGroup {
  val availible: BlockingQueue[CGroup] =
    new LinkedBlockingQueue[CGroup](
      Seq(CGroup("sandbox1"), CGroup("sandbox2")).asJava)
}

case class CGroup(id: String) {
  def exec(cmd: String) = s"cgexec -g cpuacct,memory,freezer:${id} ${cmd}"

  def getCpuTime(): Long = {
    val str = s"cgget -nvr cpuacct.usage ${id}".!!
    str.trim.toLong
  }

  def thaw(): Unit = {
    val r = s"cgset -r freezer.state=THAWED ${id}".!
    assert(r == 0);
  }

  def freeze(): Unit = {
    val r = s"cgset -r freezer.state=FROZEN ${id}".!
    assert(r == 0);
  }

  def setMemLimit(limit: Long): Unit = {
    var r0 = s"cgset -r memory.limit_in_bytes=${limit} ${id}".!
    var r1 = s"cgset -r memory.memsw.limit_in_bytes=${limit} ${id}".!
    assert(r0 == 0 && r1 == 0);
  }
}
