#!/bin/sh
mkdir -p ./players
rm -r ./players/*
for player in $(cut -f 1 -d ' ' rating.txt); do
	cp -r /home/$player/upload/ ./players/$player
done
./mill core.run
cp scoreboard.html results.html /var/www/othello/
