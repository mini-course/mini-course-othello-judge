# BullinKoth

A judge program originally for Othello AIs, though easily modifiable for other games.

`INSTRUCTIONS.md` contains the original instructions for competitors in Traditional Chinese.

## Dependencies

- `firejail`

## Features

- Glicko2 Rating Calculation
- Scoreboard Generation
- Sandboxing of Individual Submissions via `firejail` and `cgroups`
- Game Logs for Inspection

## Build

```
./mill core.compile
```

## Usage

Place submissions in `BasePath` specified in `Config.scala`, and run `./mill core.run`.

## Submission Format

The judge will execute `./build.sh` for compiling the submission, and `./run.sh PLAYER_ID` for playing the game, where `PLAYER_ID` is 0 for black and 1 for white.

## Configuration

See comments in `core/src/main/Config.scala` for details.

## Libraries

- [scala-glicko2](https://github.com/andriykuba/scala-glicko2), MIT Licensed

## License

Unlicense.
