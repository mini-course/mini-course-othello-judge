---
title: "Mini Course 2019 Othello Championship"
---

## Web Interface

- [Scoreboard](scoreboard.html)
    - 紀錄每隊的 rating 和 net score（勝場數 - 敗場數）。
    - 點擊行可進行排序。
- [Results](results.html)
    - 當前回合的結果。第 `i` 列 `j` 行表第 `i` 隊執黑對上第 `j` 隊的結果。（`O` 為黑勝，`X` 為黑敗，`-` 為平局。）
- [Logs](logs/)
    - 當前回合的棋譜。`team$i-team$j.log` 表第 `i` 隊執黑對上第 `j` 隊的過程。
    - 注意到空心字元為黑棋，實心字元為白棋。

## Test Environment

為方便測試，我們提供一個[包含 C++ 編譯器和 Python 直譯器的環境](http://hs-judge.csie.nerde.pw/othello/cygwinv4.7z)[^1]。

[^1]: Windows only。若你是 *nix 系統（需安裝 `bash`, `python3`, `g++` 等工具）的使用者，可直接執行 `./build-test.sh && ./run-test.sh X`，其中 `X = 0` 表電腦執黑，`X = 1` 表電腦執白。

下載並**右鍵解壓縮**後，可將你的原始碼（格式與上傳格式相同）放在 `cygwin/src` 下，並雙擊以下檔案執行、測試：

- `run-black.cmd`: 電腦執黑
- `run-white.cmd`: 電腦執白

我們的程式每一步都會印出棋盤的盤面（請使用深色背景的 terminal 或執行環境，否則黑棋與白棋的顏色會顛倒）。每當輪到你的時候請輸入兩個字母來代表你的動作（字母的定義與印出的棋盤上顯示的編號一樣，例如輸入 `4c` 會下在第四列第三行），一樣如果你輸入的不是合法的位置，我們的程式會隨機幫你決定。

## Upload Instructions

可以使用 [FileZilla](https://filezilla-project.org/) 等支援 SFTP 協議的程式連至 `sftp://hs-judge.csie.nerde.pw`（帳號密碼會現場發下），並將你的程式上傳至 `upload/` 目錄。注意 `upload/build.sh` 和 `upload/run.sh` 必須存在。

## Rules

- 黑白棋規則請參考[維基百科](https://zh.wikipedia.org/wiki/%E9%BB%91%E7%99%BD%E6%A3%8B#%E9%81%8A%E6%88%B2%E8%A6%8F%E5%89%87)。
- 每一回合你有 1000 毫秒（所有 CPU 核心加總）的時間可以移動。若你使用的是我們提供的模板，會自動在時間到時幫你隨機下出一著。
- 程式記憶體限制為 `4 GiB`。
- 等待對手時，你的程式不能進行運算。
- 以下情形你會被判敗：
	- 程式逾時
	- 程式 crash
	- 編譯錯誤
- 特別地，若比賽兩方均編譯錯誤，則以平手計算。
- 賽制採[雙循環制](https://zh.wikipedia.org/wiki/%E5%BE%AA%E7%8E%AF%E8%B5%9B)。
- Rating 計算採 [Glicko-2](https://zh.wikipedia.org/wiki/Glicko%E8%AF%84%E5%88%86%E7%B3%BB%E7%BB%9F)，初始參數為 `Rating=1500, Deviation=350, Volatility=0.06`。
- 總共會進行 2 個雙循環的回合，最後會選出 rating 最高的 8 強進行[單敗淘汰制](https://zh.wikipedia.org/wiki/%E5%8D%95%E8%B4%A5%E6%B7%98%E6%B1%B0%E5%88%B6)選出冠亞軍。
- 排名上若 rating 相同則比較 deviation（低者勝），若仍相同則比較 net score（高者勝）。

![](https://www.csie.ntu.edu.tw/~b07902024/othello.png)

## Support

若於課後時間有對系統的問題歡迎來信 othello@cock.li。

## C++ Instructions

請至 <https://hs-judge.csie.nerde.pw/othello/cpp.zip> 下載模板，並實作 `player.cc` 中的 `Play()`。我們提供以下的 functions：

### 位置
```cpp
struct Position;

Position pos(3, 5);
```
用來表示棋盤上的位置，`pos[0]` 代表第幾列、`pos[1]` 代表第幾行。

### 棋盤
```cpp
class Board;
```
格子由左上到右下，編號為 $(0, 0)$ 到 $(7, 7)$。黑棋先手。

### 格子狀態
```cpp
enum CellType { BLACK, WHITE, EMPTY };
```
分別代表黑棋、白棋、以及空格。

### 函式
```cpp
CellType CurPlayer();
```
回傳現在是哪個玩家的回合（`BLACK` 或 `WHITE`）。

```cpp
CellType Get(int i, int j);
```
回傳棋盤第 $(i, j)$ 格的資訊。

```cpp
bool Valid(int i, int j);
```
回傳現在的玩家是否能夠下在 $(i, j)$。

```cpp
bool Skip();
```
回傳現在的玩家是否被強制跳過，如果為 `true` ，則棋盤自動進入下一個玩家的回合。

```cpp
bool Terminated();
```
回傳是否結束。

```cpp
CellType Winner();
```
回傳贏家。


```cpp
std::vector<Position> GetAvailableMoves();
```
回傳所有現在玩家可以下的位置。

```cpp
void Place(int r, int c);
void Place(Position pos);
```
下在 $(r, c)$ 或 `pos` 的位置。

```cpp
int Count(CellType cell);
```
回傳現在棋盤有幾個格子是 `cell`。

<!-- (要在文檔放還是只放在 library 裡啊?)
### 範例
```cpp
Position Play(Board board) {
  std::vector<Position> pos = board.GetAvailableMoves();
  for (int i = 0; i < pos.size(); ++i) {
    Position p = pos[i];
    assert(board.Valid(p));
    assert(board.Valid(p[0], p[1]));
    Board copy = board;
    copy.place(p);
    if (copy.Skip()) {
      std::cout << "Skip" << std::endl;
    }
    if (copy.Count())
  }
}
```
-->

## Python Instructions

請至 <https://hs-judge.csie.nerde.pw/othello/py.zip> 下載模板，並實作 `player.py` 中的 `play()`。我們提供以下的functions：

### 棋盤
```py
class Board
```
格子由左上到右下，編號為 $(0, 0)$ 到 $(7, 7)$。黑棋先手。

### 格子狀態
```py
BLACK = 0
WHITE = 1
EMPTY = 2
```
分別代表黑棋、白棋、以及空格。

### 函式
```py
def cur_player()
```
回傳現在是哪個玩家的回合（`BLACK` 或 `WHITE`）。

```py
def get(i, j)
```
回傳棋盤第 $(i, j)$ 格的資訊。

```py
def valid(i, j)
```
回傳現在的玩家是否能夠下在 $(i, j)$。

```py
def skip()
```
回傳現在的玩家是否被強制跳過，如果為 `true` ，則棋盤自動進入下一個玩家的回合。

```py
def terminated()
```
回傳是否結束。

```py
def winner()
```
回傳贏家。

```py
def get_available_moves()
```
回傳所有現在玩家可以下的位置。

```py
def place(pos)
```
下在 `pos` 的位置。

```py
def count(cell)
```
回傳現在棋盤有幾個格子是 `cell`。

## 範例程式碼
請至 <https://hs-judge.csie.nerde.pw/othello/examples.zip> 下載範例程式碼。

## Notes

1. 如果你們實作的函式沒有在時限內 return ，你的函式會被強制終止並 return 一個隨機的動作（詳細的內容可以參考程式碼，C++ 與 Python 版本的隨機行為會一致）。
2. 如果你們的實作的函式 throw 了一個 exception ，我們也會幫你 return 一個隨機的動作。若有其他 `nothrow` 的例外導致程式終止，你會被判為輸的一方。
3. 如果你們的函式的 return 值並不是一個合法的動作，我們也會幫你隨機走一步。
4. 若使用我們提供的模板，請勿更改模板的任何內容。任何因為更改到模板而造成的問題後果自負。

<!--
## Custom Test
### C++
請使用 `g++ -std=c++14 -O3 -pthread main.cc -o main` 來編譯你的程式。
`./main 0` 或 `./main 1` 代表「電腦」是黑棋還是白棋。
### Python
請使用 `python3 main.py 0` 或 `python3 main.py 1` 來執行，分別代表「電腦」是黑棋或白棋。

### General
我們的程式每一步都會印出棋盤的盤面（請使用深色背景的 terminal 或執行環境，否則黑棋與白棋的顏色會顛倒）。每當輪到你的時候請輸入兩個字母來代表你的動作（字母的定義與印出的棋盤上顯示的編號一樣，例如輸入 `4c` 會下在第四列第三行），一樣如果你輸入的不是合法的位置，我們的程式會隨機幫你決定。
-->

## Custom Languages

若你想要使用其他的語言，請提前通知助教，讓我們安裝相關編譯器與環境。

另外，你會需要實作以下檔案：

- `build.sh`: 進行編譯的 shell script
- `run.sh`: 執行你的程式的 shell script，需接收一個為 `0` 或 `1` 的 argument 表你執黑或執白。

你的程式需要以標準輸入輸出與我們的評測程式溝通。每一手輸出入的字串均為 `x y` 形式的一行（`0 <= x, y < 8`），表示該手下的位置。

注意到你下每一手都需要 flush 標準輸出，詳細步驟請參考你使用的語言的文檔。

另外，請注意若另一方無處可下，你需要連續輸出數手。
