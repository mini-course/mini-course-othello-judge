import mill._, scalalib._, scalafmt._

object core extends ScalaModule with ScalafmtModule {
	def scalaVersion = "2.13.1"
	def scalacOptions = Seq("-deprecation", "-Ybackend-parallelism", "4")
	def ivyDeps = Agg(ivy"com.lihaoyi::scalatags:0.7.0")
}
